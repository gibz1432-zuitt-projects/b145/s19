let students = [];
let sectionedStudents = [];

function addStudent(name){
students.push(name);
console.log(name + ' has been added to the list of students');
}

addStudent('Zyrus');
addStudent('John');
addStudent('Marie');


function countStudents(){
	console.log('This Class has a total of ' + students.length + ' enrolled students');
}
countStudents();


function printStudents(){
	students = students.sort();
	console.log(students);
	students.forEach(function(student){
		console.log(student);
	})
}
printStudents();


function findStudent(keyword){
	let matches = students.filter(function(student){
		return student.toLowerCase().includes(keyword.toLowerCase());
	});
// 	console.log(typeof matches)
if(matches.length === 1){
	console.log(matches[0] + ' is Enrolled')
} else if(matches.length > 1){
	console.log('Multiple Students Matches this Keyword')
} else{
	console.log('No students matches this keyword')
}
}
findStudent('r');



function addSection (section) {
	sectionedStudents = students.map(function(student){
		return student + ' is part of section: ' + section;
	})
	console.log(sectionedStudents);
}
addSection('5');


function removeStudent(name){
	let result = students.indexOf(name)
	console.log(result);
	if (result >=0 ) {
		students.splice(result,1)
		console.log(students)
		console.log(name + ' was removed');
	} else {
		console.log('No student was Removed');
	}
	
}

removeStudent('Zyrus');